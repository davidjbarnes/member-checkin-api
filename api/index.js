const express = require("express");
const http = require("http");
const socketIo = require("socket.io");
const port = process.env.PORT || 4001;
const app = express();
const server = http.createServer(app);
const io = socketIo(server);

const client = require("./client/checkin");

app.use(express.static("api/public"));

app.get("/", function (req, res) {
    var id = req.query.id;

    client.publish(id).then((data) => {
        res.send(data);
    });
});

io.on("connection", socket => {
  console.log("New client connected");

  client.subscribe(socket);

  setInterval(() => {
    socket.emit("PING", "PING");
  }, 2000);

  socket.on("disconnect", () => console.log("Client disconnected"));
});

server.listen(port, () => console.log(`Listening on port ${port}`));