var amqp = require('amqplib/callback_api');

const publish = (msg) => {
    return new Promise((resolve, reject) => {
        amqp.connect('amqp://vE3zU1aQ:dYtqOMqtg0A35xwjpGv87lNer26c-Tvm@energetic-charlock-1.bigwig.lshift.net:10272/_1xs5HKhTOb9', function(err, conn) {
            conn.createChannel(function(err, ch) {
                var ex = 'checkins';

                ch.assertExchange(ex, 'fanout', {durable: false});
                ch.publish(ex, '', new Buffer(msg));

                if(err) { console.error(err); }

                resolve({
                    ex:ex,
                    msg:msg
                });
            });
        });
    }).catch((err) => {
        if(err) { console.error(err); }
    });
};

const subscribe = (socket) => {
    amqp.connect('amqp://vE3zU1aQ:dYtqOMqtg0A35xwjpGv87lNer26c-Tvm@energetic-charlock-1.bigwig.lshift.net:10273/_1xs5HKhTOb9', function(err, conn) {
        conn.createChannel(function(err, ch) {
            var ex = 'checkins';

            ch.assertExchange(ex, 'fanout', {durable: false});
            ch.assertQueue('', {exclusive: false}, function(err, q) {
                ch.bindQueue(q.queue, ex, '');
                ch.consume(q.queue, function(msg) {
                    socket.emit("checkin-event", msg.content.toString());
                }, { noAck: true });
            });

            if(err) { console.error(err); }
        });
    });
};

module.exports = {
    publish,
    subscribe
};